import {View, Text} from 'react-native';
import React from 'react';

const Header = ({title}) => {
  return (
    <View
      style={{
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        // marginBottom: 10,
      }}>
      <Text
        style={{
          fontWeight: 'bold',
          fontSize: 20,
          color: '#ffffff',
          letterSpacing: 1,
        }}>
        {title}
      </Text>
    </View>
  );
};

export default Header;
