import {View, Text, Platform, TextInput, StyleSheet} from 'react-native';
import React, {useState} from 'react';

const textInput = ({placeholders, onChangeText, values, heights}) => {
  const [lokalText, setLokalText] = useState('');
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          borderBottomWidth: 0.5,
          height: 40,
          width: '90%',
          marginLeft: 10,
        }}>
        <TextInput
          placeholder={placeholders}
          // placeholderTextColor={'black'}
          value={values}
          onChangeText={lokalText => {
            setLokalText(lokalText);
            onChangeText(lokalText);
          }}
        />
      </View>
    </View>
  );
};

const style = StyleSheet;

export default textInput;
