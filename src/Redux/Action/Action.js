import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const getData = (email, password) => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };

  return async dispatch => {
    const data = {
      email,
      password,
    };
    dispatch({
      type: 'LOGIN',
      data: data,
    });
    dataToken(JSON.stringify(data));
  };
};

export const Registerss = (dataUser, dataEmail, dataPass, dataPhone) => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return async dispatch => {
    var data = JSON.stringify({
      name: dataUser,
      email: dataEmail,
      pass: dataPass,
      phone: dataPhone,
    });

    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        const data = JSON.stringify(response.data);
        dispatch({
          type: 'REGISTER_SUCCESS',
          data: data,
        });
        dataToken(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const Histo = () => {
  return async dispatch => {
    var data = '';

    var config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {},
      data: data,
    };
    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        const data = response.data;
        dispatch({
          type: 'HISTORY',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const Transaksi = (dataAmount, dataSender, dataTarget, dataType) => {
  return async dispatch => {
    var data = JSON.stringify({
      amount: dataAmount,
      sender: dataSender,
      target: dataTarget,
      type: dataType,
    });

    var config = {
      method: 'post',
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'content-type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        const data = JSON.stringify(response.data);
        console.log(data, 'berhasil');
        dispatch({
          type: 'GET_TRANSAKSI',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
    return true;
  };
};

export const LogOut = () => {
  return dispatch => {
    const data = '';
    dispatch({
      type: 'SIGN_OUT',
      data: data,
    });
  };
};
