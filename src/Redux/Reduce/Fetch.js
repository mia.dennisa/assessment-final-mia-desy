const initialState = {
  DataStore: [],
  History: [],
};

const Fetch = (state = initialState, action) => {
  switch (action.type) {
    case 'GET-DATA':
      return {
        ...state,
        DataStore: action.data,
      };
    case 'HISTORY':
      return {
        ...state,
        History: action.data,
      };
    default:
      return state;
  }
};
export default Fetch;
