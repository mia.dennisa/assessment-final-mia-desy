import React, {useEffect, useState} from 'react';
import Home from '../Home/Home';
import Login from '../Home/Login';
import {NavigationContainer} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import History from '../Home/History';
import Filter from '../Home/Filter';
import Transfer from '../Home/Trasfer';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Pulsa from '../Home/Pulsa';
import Topup from '../Home/Topup';
import Register from '../Home/Register';
import {Screen} from 'react-native-screens';
import SplashScreen from '../SplashSceen/Splashcreen';

const Stack = createNativeStackNavigator();
const AppStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: 'none'}}>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen name="History" component={History} />
      <Stack.Screen name="Transfer" component={Transfer} />
      <Stack.Screen name="Filter" component={Filter} />
      <Stack.Screen name="Pulsa" component={Pulsa} />
      <Stack.Screen name="Topup" component={Topup} />
    </Stack.Navigator>
  );
};

const AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Splashscreen" component={SplashScreen} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
    </Stack.Navigator>
  );
};

function Router() {
  const [tokens, setTokens] = useState(null);
  const UpdateData = useSelector(state => state.Auth.AuthData);

  const DataUser = async () => {
    const token = await AsyncStorage.getItem('Token');
    setTokens(JSON.parse(token));
  };
  useEffect(() => {
    DataUser();
  }, [UpdateData]);
  return (
    <NavigationContainer>
      {tokens !== null ? <AppStack /> : <AuthStack />}
    </NavigationContainer>
  );
}

export default Router;
