import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useState} from 'react';
import {
  View,
  TextInput,
  Alert,
  StyleSheet,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useDispatch, useSelector} from 'react-redux';
import {getData} from '../Redux/Action/Action';
import iLogi from '../Image/logut.png';
import Register from './Register';

const Validasi = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [disabled, setDisables] = useState(false);
  const dispatch = useDispatch();

  const ValidatorButton = () => {
    return email !== '' && password !== ''
      ? setDisables(false)
      : setDisables(true);
  };

  useEffect(() => {
    ValidatorButton();
  }, [email, password]);

  const state = useSelector(state => state.Auth);
  console.log(state, 'Data');

  const validateEmail = () => {
    if (email == 'M@gmail.com') {
      console.log(email);
      return true;
    } else {
      // console;
      if (email === '') {
        console.log('Error', 'Email tidak boleh kosong');
        return false;
      }

      if (email !== setEmail) {
        console.error('Eror', 'Email Failed');
        return false;
      }
    }
    return false;
  };

  const validatePassword = () => {
    if (password == 'Miahi666') {
      console.log(password);
      return true;
    } else {
      // console;
      if (password === '') {
        console.log('Error', 'Password tidak boleh kosong');
        return false;
      }
      if (password !== setPassword) {
        console.error('Eror', 'Password Failed');
        return false;
      }
    }
    return false;
  };

  async function Logino() {
    try {
      dispatch(getData(email, password));
    } catch (e) {
      console.log('errors', e);
    }
  }

  const login = () => {
    if (validateEmail() && validatePassword()) {
      Alert.alert('SELAMAT', 'LOGIN SUKSES', [
        {text: 'OK', onPress: () => Logino()},
      ]);
    }
  };

  return (
    <ScrollView>
      <View style={styles.background}>
        <ImageBackground source={require('../Image/back.png')}>
          <SafeAreaView style={styles.container}>
            <View style={{alignItems: 'center'}}>
              <View
                style={{
                  alignSelf: 'flex-start',
                }}>
                <Text
                  style={{
                    fontSize: 60,
                    fontFamily: 'cursive',
                    marginTop: 60,
                    fontWeight: 'bold',
                    color: 'black',
                  }}>
                  Walcome
                </Text>
                <Text
                  style={{
                    fontSize: 60,
                    fontFamily: 'cursive',
                    marginLeft: 110,
                    fontWeight: 'bold',
                    color: 'black',
                  }}>
                  Back
                </Text>
              </View>
            </View>
            <View style={{alignItems: 'center'}}>
              <View style={styles.email}>
                <View>
                  <TextInput
                    placeholder="Email"
                    value={email}
                    onChangeText={text => setEmail(text)}
                  />
                </View>
              </View>
            </View>

            <View style={{alignItems: 'center'}}>
              <View style={styles.pass}>
                <View>
                  <TextInput
                    placeholder="Password"
                    secureTextEntry
                    value={password}
                    onChangeText={text => setPassword(text)}
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  marginTop: 30,
                  fontSize: 30,
                  fontWeight: 'bold',
                  color: '#00b8e6',
                  marginBottom: 50,
                  marginLeft: 10,
                  marginRight: 30,
                }}>
                Sign In
              </Text>
              <TouchableOpacity
                title="Submit"
                onPress={() => {
                  login();
                }}>
                <View style={{marginLeft: 140, marginTop: 25}}>
                  <Image source={iLogi} style={{height: 60, width: 60}} />
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  marginRight: 80,
                  fontSize: 20,
                }}>
                <Text> Don't have an account ?</Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Register');
                }}>
                <Text
                  style={{
                    fontSize: 20,
                    color: '#00b8e6',
                    fontWeight: 'bold',
                    borderBottomWidth: 1,
                    borderBottomColor: '#00b8e6',
                  }}>
                  Sign Up
                </Text>
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </ImageBackground>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 40,
  },
  background: {
    flex: 1,
    resizeMode: 'cover',
    flexDirection: 'row-reverse',
  },
  email: {
    borderBottomWidth: 0.5,
    height: 45,
    width: '90%',
    marginBottom: 15,
    marginTop: 100,
  },
  pass: {
    borderBottomWidth: 0.5,
    height: 45,
    width: '90%',
    marginBottom: 20,
  },
});

export default Validasi;
