import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  BackHandler,
  ImageBackground,
} from 'react-native';

import iLogo from '../Image/logut.png';
import iHisto from '../Image/history.png';
import iPul from '../Image/pulsa.png';
import icard from '../Image/card1.png';
import icard1 from '../Image/download.png';
import iTrans from '../Image/transaksi.png';
import logut from '../Image/logout.gif';
import icoin from '../Image/coin.png';
import isale from '../Image/sale.png';
import idepo from '../Image/depo.png';
import {useDispatch} from 'react-redux';
import {LogOut} from '../Redux/Action/Action';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Home = ({navigation}) => {
  const HandlerBack = () => {
    //BackHandler.exitApp()
    navigation.goBack();
    return true;
  };
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);
  const [Change, setChange] = useState(0);
  const ChangeColor = itemID => {
    setChange(currentItem => (currentItem === itemID ? itemID : itemID));
  };

  const tampilan = [
    {label: 'Promo', id: 0},
    {label: 'Tabungan', id: 1},
    {label: 'Investasi', id: 2},
    {label: 'Deposit', id: 3},
  ];

  const dispatch = useDispatch();
  async function logout() {
    try {
      console.log('masuk');
      dispatch(LogOut());
      await AsyncStorage.removeItem('Token');
    } catch (e) {
      console.log('erro', e);
    }
  }

  return (
    <ScrollView>
      <View style={styles.background}>
        <ImageBackground source={require('../Image/back.png')}>
          <View style={styles.container}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image
                source={icard}
                style={{
                  marginTop: 40,
                  width: 280,
                  height: 250,
                }}
              />
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                elevation: 10,
                paddingVertical: 20,
                marginTop: 10,
                justifyContent: 'space-evenly',
              }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Transfer');
                }}>
                <View style={{alignItems: 'center', width: 100}}>
                  <View style={styles.trans}>
                    <Image
                      source={iTrans}
                      style={{height: 50, width: 60, resizeMode: 'center'}}
                    />
                  </View>
                  <Text style={{fontWeight: 'bold', color: 'black'}}>
                    Transfer
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Pulsa');
                }}>
                <View style={{alignItems: 'center', width: 100}}>
                  <View style={styles.pulsa}>
                    <Image
                      source={iPul}
                      style={{height: 40, width: 150, resizeMode: 'contain'}}
                    />
                  </View>
                  <Text style={{fontWeight: 'bold', color: 'black'}}>
                    Pulsa
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('History');
                }}>
                <View style={{alignItems: 'center', width: 100}}>
                  <View style={styles.histo}>
                    <Image
                      source={iHisto}
                      style={{height: 40, width: 150, resizeMode: 'contain'}}
                    />
                  </View>
                  <Text style={{fontWeight: 'bold', color: 'black'}}>
                    History
                  </Text>
                </View>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flexDirection: 'row',
                elevation: 10,
                paddingVertical: 20,
                // marginLeft: 235,
                justifyContent: 'space-evenly',
              }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Topup');
                }}>
                <View style={{alignItems: 'center', width: 100}}>
                  <View style={styles.Topup}>
                    <Image
                      source={iTrans}
                      style={{height: 50, width: 60, resizeMode: 'center'}}
                    />
                  </View>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      color: 'black',
                      marginRight: 15,
                    }}>
                    Topup
                  </Text>
                </View>
              </TouchableOpacity>

              <View
                onTouchEnd={async () => {
                  logout();
                }}>
                <View style={styles.logout}>
                  <Image
                    source={logut}
                    style={{
                      width: 40,
                      resizeMode: 'contain',
                      // marginLeft: 10,
                    }}></Image>
                </View>
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: 'black',
                    marginLeft: 4,
                  }}>
                  Log Out
                </Text>
              </View>
            </View>

            <View>
              <Text
                style={{
                  fontWeight: 'bold',
                  color: 'black',
                  fontSize: 20,
                }}>
                Insights
              </Text>
            </View>

            <View>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                {tampilan.map((item, index) => {
                  const colors = Change == item.id ? '#000' : 'grey';
                  return (
                    <TouchableOpacity
                      key={index}
                      onPress={() => {
                        ChangeColor(item.id);
                      }}>
                      <View style={styles.tampilan}>
                        <Text
                          style={{
                            color: colors,
                          }}>
                          {item.label}
                        </Text>
                        <View style={{alignItems: 'flex-end'}}>
                          <Image source={idepo}></Image>
                        </View>
                      </View>
                    </TouchableOpacity>
                  );
                })}
              </ScrollView>
            </View>
          </View>
        </ImageBackground>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  background: {
    flex: 1,
    resizeMode: 'cover',
    flexDirection: 'row',
  },
  trans: {
    height: 50,
    width: 50,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  pulsa: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  histo: {
    height: 50,
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  Topup: {
    height: 50,
    width: 50,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  logout: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  tampilan: (click, data) => ({
    backgroundColor: click !== data ? '#f2f2f2' : '#f2f2f2',
    borderColor: click !== data ? '#f2f2f2' : '#f2f2f2',
  }),
});

export default Home;
