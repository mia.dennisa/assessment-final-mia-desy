import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  ScrollView,
  ImageBackground,
} from 'react-native';
import Header from '../Component/Header';
import React, {useState, useEffect} from 'react';
import Search from '../Home/Search';
import Filter from '../Home/Filter';
import {TextInput} from 'react-native-paper';
import {Histo} from '../Redux/Action/Action';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch, useSelector} from 'react-redux';

const History = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  const [ketikan, setKetikan] = useState('');
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [screens, setScreens] = useState(1);

  const dataFilter = async data => {
    const dataArray = Object.keys(data).map(key => ({
      sender: data[key].sender,
      target: data[key].target,
      amount: data[key].amount,
      type: data[key].type,
    }));
    setData(dataArray);
  };

  const dispatch = useDispatch();
  const historyyyyy = () => {
    try {
      setLoading(true);
      dispatch(Histo());
      setTimeout(() => {
        setLoading(false);
      }, 2000);
    } catch (e) {
      console.log('error', e);
    }
  };

  const states = useSelector(state => state.Fetch?.History);
  const result = Object.entries(states);

  useEffect(() => {
    historyyyyy();
    dataFilter(states);
  }, []);

  const sortAscending = () => {
    const sortedData = [...data].sort((a, b) => a.amount - b.amount);
    setData(sortedData);
  };

  const sortDescending = () => {
    const sortedData = [...data].sort((a, b) => b.amount - a.amount);
    setData(sortedData);
  };

  if (loading === true) {
    return <Text>Loading...</Text>;
  }
  return (
    <View style={styles.background}>
      <ImageBackground source={require('../Image/back.png')}>
        <View style={styles.container}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
              marginBottom: 10,
            }}>
            <Search
              KetikanUser={ketikan}
              navigation={navigation}
              UbahKetikanUser={e => {
                setKetikan(e);
                setScreens(1);
              }}
            />
            <Filter
              onPress2={() => {
                sortAscending();
                setScreens(2);
              }}
              onPress1={() => {
                sortDescending();
                setScreens(2);
              }}
            />
          </View>
          <View>
            {screens == 1 ? (
              <ScrollView>
                {result?.map((e, index) => {
                  const filter = e.filter(item => {
                    return (
                      item?.type
                        ?.toLowerCase()
                        .includes(ketikan?.toLowerCase()) ||
                      item?.amount
                        ?.toString()
                        .toLowerCase()
                        .includes(ketikan?.toLowerCase()) ||
                      item?.sender
                        ?.toString()
                        .toLowerCase()
                        .includes(ketikan?.toLowerCase()) ||
                      item?.target
                        ?.toString()
                        .toLowerCase()
                        .includes(ketikan?.toLowerCase())
                    );
                  });

                  return (
                    <View style={styles.body}>
                      {filter.map((es, index) => {
                        return (
                          // kotak
                          <View style={styles.list} key={index}>
                            <View
                              style={{
                                flexDirection: 'column',
                              }}>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                }}>
                                <Text style={styles.tipe}>{es.type}</Text>
                                <Text
                                  style={{
                                    color: 'white',
                                    backgroundColor: '#33bbff',
                                    borderRadius: 5,
                                    fontWeight: 'bold',
                                    padding: 5,
                                    fontSize: 11,
                                    marginBottom: 5,
                                    justifyContent: 'flex-end',
                                    alignItems: 'flex-end',
                                  }}>
                                  Berhasil
                                </Text>
                              </View>

                              <View
                                style={{
                                  flexDirection: 'row',
                                }}>
                                {/* gambar */}
                                <View
                                  style={{
                                    width: '10%',
                                  }}></View>
                                {/* data */}
                                <View
                                  style={{
                                    width: '80%',
                                  }}>
                                  <Text style={styles.data}>
                                    Pengirim : {es.sender}
                                  </Text>
                                  <Text style={styles.data}>
                                    Penerima : {es.target}
                                  </Text>
                                  <Text style={styles.data}>
                                    Jumlah : {es.amount}
                                  </Text>
                                </View>
                              </View>
                            </View>
                          </View>
                        );
                      })}
                    </View>
                  );
                })}
              </ScrollView>
            ) : (
              <ScrollView>
                {data.length > 0 ? (
                  data.map((es, index) => (
                    <View style={styles.list} key={index}>
                      <View
                        style={{
                          flexDirection: 'column',
                        }}>
                        {/* judul&ket */}
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                          }}>
                          <Text style={styles.tipe}>{es.type}</Text>
                          <Text
                            style={{
                              color: '#00b2d6',
                              backgroundColor: '#b3d9ff',
                              borderRadius: 5,
                              fontWeight: 'bold',
                              padding: 5,
                              fontSize: 11,
                              marginBottom: 5,
                              justifyContent: 'flex-end',
                              alignItems: 'flex-end',
                            }}>
                            Berhasil
                          </Text>
                        </View>

                        {/* ROW */}
                        <View
                          style={{
                            flexDirection: 'row',
                          }}>
                          {/* gambar */}
                          <View
                            style={{
                              width: '10%',
                            }}></View>
                          {/* data */}
                          <View
                            style={{
                              width: '80%',
                            }}>
                            <Text style={styles.data}>
                              Pengirim : {es.sender}
                            </Text>
                            <Text style={styles.data}>
                              Penerima : {es.target}
                            </Text>
                            <Text style={styles.data}>
                              Jumlah : {es.amount}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  ))
                ) : (
                  <Text></Text>
                )}
              </ScrollView>
            )}
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    marginRight: 18,
  },
  background: {
    flex: 1,
    resizeMode: 'cover',
    flexDirection: 'row-reverse',
  },
  list: {
    justifyContent: 'center',
    alignContent: 'center',
    padding: 8,
    elevation: 1.5,
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    marginTop: 5,
  },
  body: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  data: {
    color: 'black',
    fontFamily: 'Montserrat-Regular',
  },
  tipe: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'black',
  },
});

export default History;
