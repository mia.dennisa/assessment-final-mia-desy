import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Alert,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {RadioButton, TextInput} from 'react-native-paper';
import {useDispatch, useSelector} from 'react-redux';
import {Transaksi} from '../Redux/Action/Action';

const Pulsa = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  const [disables, setDisables] = useState(false);
  const [dataAmount, setDataAmount] = useState('');
  const [dataSender, setDataSender] = useState('NMalhNh5LPYMCq7uuU');
  const [dataTarget, setDataTarget] = useState('');
  const [dataType, setDataType] = useState('');
  const dispatch = useDispatch();
  const state = useSelector(state => state.fetch);

  async function Transaks() {
    dispatch(Transaksi(dataAmount, dataSender, dataTarget, dataType));
  }

  const ValidatorButton = () => {
    return dataAmount !== '' && dataTarget !== '' && dataType !== ''
      ? setDisables(false)
      : setDisables(true);
  };
  useEffect(() => {
    ValidatorButton();
  }, [dataAmount, dataTarget, dataType]);

  const rupiah = x => {
    return x
      ?.toString()
      .replace(/\./g, '')
      .replace(/(\d)(?=(\d{3})+$)/g, '$1' + '.');
  };

  function total() {
    Transaks(dataAmount, dataSender, dataTarget, dataType);
    Alert.alert(`Berhasil Beli Pulsa dengan Nominal ${dataAmount}`);
    navigation.navigate('History');
  }
  return (
    <ScrollView>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
        style={{flex: 1}}>
        <SafeAreaView style={style.container}>
          <View>
            <Text
              style={{
                marginTop: 25,
                marginBottom: 45,
                textAlign: 'center',
                fontWeight: 'bold',
                color: 'black',
                fontSize: 20,
              }}>
              Transaksi
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              marginTop: 10,
              marginLeft: 15,
              alignItems: 'center',
            }}>
            <RadioButton
              value="Pulsa"
              status={dataType === 'pulsa' ? 'checked' : 'unchecked'}
              onPress={() => setDataType('pulsa')}
            />
            <Text
              style={{
                color: 'black',
                fontSize: 17,
                fontWeight: 'bold',
                marginLeft: 10,
              }}>
              Pengisian Pulsa
            </Text>
          </View>

          <View style={{marginVertical: 5}}>
            <TextInput
              disabled={true}
              placeholder={'Miade'}
              values={dataSender}
              onChangeText={tg => {
                setDataSender(tg);
              }}
              style={{
                height: 50,
                borderWidth: 1.5,
                borderRadius: 5,
                marginHorizontal: 20,
                paddingHorizontal: 10,
                backgroundColor: '#e6e6e6',
              }}
            />
          </View>

          <View style={{marginVertical: 15}}>
            <TextInput
              placeholder={'Nominal'}
              values={dataAmount}
              onChangeText={am => {
                setDataAmount(rupiah(am));
              }}
              style={{
                height: 50,
                borderWidth: 1.5,
                borderRadius: 5,
                marginHorizontal: 20,
                paddingHorizontal: 10,
                backgroundColor: '#e6e6e6',
              }}
            />
          </View>

          <View style={{marginVertical: 5}}>
            <TextInput
              placeholder={'No. Tujuan'}
              values={dataTarget}
              onChangeText={tg => {
                setDataTarget(tg);
              }}
              style={{
                height: 50,
                borderWidth: 1.5,
                borderRadius: 5,
                marginHorizontal: 20,
                paddingHorizontal: 10,
                backgroundColor: '#e6e6e6',
              }}
            />
          </View>

          <View>
            <TouchableOpacity
              disabled={disables}
              onPress={() => {
                total();
              }}
              style={{
                backgroundColor: disables == false ? '#e6e6e6' : '#d9d9d9',
                marginHorizontal: 150,
                paddingVertical: 15,
                marginTop: 25,
                borderRadius: 100,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: 'black',
                  fontSize: 20,
                  fontWeight: 'bold',
                }}>
                Bayar
              </Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
});

export default Pulsa;
