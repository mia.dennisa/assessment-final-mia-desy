import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  ImageBackground,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import regis from '../Image/Login.png';
import {Registerss, SignUp} from '../Redux/Action/Action';
import TextInputReg from '../Component/TextInputReg';
import Home from './Home';

const Register = ({navigation}) => {
  const [dataUser, setdataUser] = useState('');
  const [dataEmail, setdataEmail] = useState('');
  const [dataPhone, setdataPhone] = useState('');
  const [dataPass, setdataPass] = useState('');
  console.log(dataPass);
  const [disabled, setDisables] = useState(false);
  const dispatch = useDispatch();

  async function Regis() {
    try {
      dispatch(Registerss(dataUser, dataPhone, dataPass, dataEmail));
    } catch (e) {
      console.log('errors', e);
    }
  }
  const ValidatorButton = () => {
    return dataEmail !== '' &&
      dataUser !== '' &&
      dataPass !== '' &&
      dataPhone !== ''
      ? setDisables(false)
      : setDisables(true);
  };
  useEffect(() => {
    ValidatorButton();
  }, [dataEmail, dataPass, dataPhone, dataUser]);

  const state = useSelector(state => state.Auth);
  console.log(state, 'Data diri');

  const validateUser = () => {
    if (dataUser === '') {
      Alert.alert('Error', 'Isi Nama');
      return false;
    }
    return true;
  };

  const validatePhone = () => {
    if (dataPhone === '') {
      Alert.alert('Error', 'Isi nomer Hp');
      return false;
    }
    if (dataPhone.length > 12) {
      Alert.alert('Error', 'Nomer tlp harus 12 angka');
      return false;
    }
    if (dataPhone.length < 12) {
      Alert.alert('Error', 'Nomer tlp harus 12 angka');
      return false;
    }
    if (!/^-?\d+$/.test(dataPhone)) {
      Alert.alert('Error', 'Nomer harus terdiri dari  angka');
      return false;
    }
    return true;
  };

  const validateEmail = () => {
    if (dataEmail === '') {
      Alert.alert('Error', 'Email tidak boleh kosong');
      return false;
    }
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(dataEmail)) {
      Alert.alert('Error', 'Email tidak valid');
      return false;
    }
    return true;
  };

  const validatePassword = () => {
    if (dataPass === '') {
      Alert.alert('Error', 'Password tidak boleh kosong');
      return false;
    } else if (dataPass.length > 8) {
      Alert.alert('Error', 'Password minimal 8 karakter');
      return false;
    } else if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/.test(dataPass)) {
      Alert.alert(
        'Password harus terdiri dari huruf besar, huruf kecil, dan angka',
      );
      return false;
    }
    return true;
  };

  const Regisyuu = () => {
    if (
      validateUser() &&
      validatePhone() &&
      validateEmail() &&
      validatePassword()
    ) {
      Alert.alert('SELAMAT', 'REGISTRASI SUKSES', [
        {text: 'OK', onPress: () => Regis()},
      ]);
    }
  };

  return (
    <ScrollView>
      <View style={styles.background}>
        <ImageBackground source={require('../Image/back.png')}>
          <SafeAreaView style={styles.container}>
            <View>
              <View>
                <View
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 12,
                  }}>
                  <Text
                    style={{
                      fontSize: 60,
                      fontFamily: 'cursive',
                      marginTop: 60,
                      fontWeight: 'bold',
                      color: 'black',
                    }}>
                    Create
                  </Text>
                  <Text
                    style={{
                      fontSize: 60,
                      fontFamily: 'cursive',
                      marginLeft: 70,
                      fontWeight: 'bold',
                      color: 'black',
                    }}>
                    Account
                  </Text>
                </View>

                <View style={{marginVertical: 5}}>
                  <TextInputReg
                    placeholders={'Username'}
                    value={dataUser}
                    onChangeText={text => {
                      setdataUser(text);
                    }}
                    heights={undefined}
                  />
                </View>

                <View style={{marginVertical: 5}}>
                  <TextInputReg
                    placeholders={'Phone'}
                    value={dataPhone}
                    onChangeText={text => {
                      setdataPhone(text);
                    }}
                    heights={undefined}
                  />
                </View>

                <View style={{marginVertical: 5}}>
                  <TextInputReg
                    placeholders={'Email'}
                    value={dataEmail}
                    onChangeText={text => {
                      setdataEmail(text);
                    }}
                    heights={undefined}
                  />
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomWidth: 0.5,
                    height: 40,
                    width: '90%',
                    marginBottom: 15,
                    marginLeft: 10,
                  }}>
                  <TextInput
                    Label={'Password'}
                    placeholder="Pass"
                    value={dataPass}
                    secureTextEntry={true}
                    onChangeText={text => {
                      setdataPass(text);
                    }}
                  />
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <Text
                  style={{
                    marginTop: 30,
                    fontSize: 30,
                    fontWeight: 'bold',
                    color: '#00b8e6',
                    marginBottom: 50,
                    marginLeft: 10,
                    marginRight: 30,
                  }}>
                  Sign Up
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    Regisyuu();
                  }}>
                  <View style={{marginLeft: 110, marginTop: 30}}>
                    <Image source={regis} style={{height: 60, width: 60}} />
                  </View>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginRight: 230,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    ValidatorButton();
                    navigation.navigate('Login');
                  }}>
                  <Text
                    style={{
                      fontSize: 20,
                      color: '#00b8e6',
                      fontWeight: 'bold',
                      borderBottomWidth: 1,
                      borderBottomColor: '#00b8e6',
                    }}>
                    Sign In
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </SafeAreaView>
        </ImageBackground>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 50,
  },
  background: {
    flex: 1,
    resizeMode: 'cover',
    flexDirection: 'row',
  },
});

export default Register;
