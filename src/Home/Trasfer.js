import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Alert,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {RadioButton, TextInput} from 'react-native-paper';
import {Transaksi} from '../Redux/Action/Action';
import {useDispatch, useSelector} from 'react-redux';

const Transfer = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  const [disables, setDisables] = useState(false);
  const [dataAmount, setDataAmount] = useState('');
  const [dataSender, setDataSender] = useState('NMalhNh5LPYMCq7uuU');
  const [dataTarget, setDataTarget] = useState('');
  const [dataType, setDataType] = useState('');
  // const [transaksi, setTransaksi] = useState('Transfer');
  const dispatch = useDispatch();
  const state = useSelector(state => state.fetch);

  async function Transaks() {
    dispatch(Transaksi(dataAmount, dataSender, dataTarget, dataType));
  }

  const ValidatorButton = () => {
    return dataAmount !== '' && dataTarget !== '' && dataType !== ''
      ? setDisables(false)
      : setDisables(true);
  };
  useEffect(() => {
    ValidatorButton();
  }, [dataAmount, dataTarget, dataType]);

  const rupiah = x => {
    return x
      ?.toString()
      .replace(/\./g, '')
      .replace(/(\d)(?=(\d{3})+$)/g, '$1' + '.');
  };

  function total() {
    Transaks(dataAmount, dataSender, dataTarget, dataType);
    Alert.alert(`Berhasil Transfer dengan Nominal ${dataAmount}`);
    navigation.navigate('History');
  }
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      style={{flex: 1}}>
      <SafeAreaView style={style.container}>
        <ScrollView>
          <View>
            <Text
              style={{
                marginTop: 25,
                marginBottom: 45,
                textAlign: 'center',
                fontWeight: 'bold',
                color: 'black',
                fontSize: 20,
              }}>
              Transaksi
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 10,
              marginLeft: 15,
              alignItems: 'center',
            }}>
            <RadioButton
              value="Transfer"
              status={dataType === 'Transfer' ? 'checked' : 'unchecked'}
              onPress={() => setDataType('Transfer')}
            />
            <Text
              style={{
                color: 'black',
                fontSize: 17,
                fontWeight: 'bold',
                marginLeft: 10,
              }}>
              Transfer Antara Bank
            </Text>
          </View>

          <View style={{marginVertical: 5}}>
            <TextInput
              disabled={true}
              placeholder={'NMalhNh5LPYMCq7uuU'}
              values={dataSender}
              onChangeText={tg => {
                setDataSender(tg);
              }}
              style={{
                height: 50,
                borderWidth: 1.5,
                borderRadius: 5,
                marginHorizontal: 20,
                paddingHorizontal: 10,
                backgroundColor: '#e6e6e6',
              }}
            />
          </View>

          <View style={{marginVertical: 15}}>
            <TextInput
              placeholder={'Nominal'}
              values={dataAmount}
              onChangeText={am => {
                setDataAmount(rupiah(am));
              }}
              style={{
                height: 50,
                borderWidth: 1.5,
                borderRadius: 5,
                marginHorizontal: 20,
                paddingHorizontal: 10,
                backgroundColor: '#e6e6e6',
              }}
            />
          </View>

          <View style={{marginVertical: 5}}>
            <TextInput
              placeholder={'Rekening'}
              values={dataTarget}
              onChangeText={tg => {
                setDataTarget(tg);
              }}
              style={{
                height: 50,
                borderWidth: 1.5,
                borderRadius: 5,
                marginHorizontal: 20,
                paddingHorizontal: 10,
                backgroundColor: '#e6e6e6',
              }}
            />
          </View>

          <View>
            <TouchableOpacity
              disabled={disables}
              onPress={() => {
                total();
              }}
              style={{
                backgroundColor: disables == false ? '#e6e6e6' : '#d9d9d9',
                marginHorizontal: 150,
                paddingVertical: 15,
                marginTop: 25,
                borderRadius: 100,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: 'black',
                  fontSize: 20,
                  fontWeight: 'bold',
                }}>
                Bayar
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e6f7ff',
  },
});

export default Transfer;
