import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import React, {useState, useEffect} from 'react';

const Filter = ({onPress1, onPress2}) => {
  const [data, setData] = useState([]);

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity onPress={onPress1}>
          <View
            style={{
              width: '5%',
              alignItems: 'flex-start',
              justifyContent: 'flex-start',
            }}>
            <Image
              source={{
                uri: 'https://img.icons8.com/pastel-glyph/512/sort-amount-up.png',
              }}
              style={{
                height: 23,
                width: 40,
                resizeMode: 'contain',
                marginTop: 5,
                marginRight: -6,
              }}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={onPress2}>
          <View
            style={{
              width: '5%',
              alignItems: 'flex-start',
              justifyContent: 'flex-start',
            }}>
            <Image
              source={{
                uri: 'https://img.icons8.com/pastel-glyph/512/generic-sorting.png',
              }}
              style={{
                height: 23,
                width: 40,
                resizeMode: 'contain',
                marginTop: 5,
              }}
            />
          </View>
        </TouchableOpacity>
      </View>
      <View></View>
    </View>
  );
};

const styles = StyleSheet.create({
  list: {
    justifyContent: 'center',
    alignContent: 'center',
    padding: 8,
    elevation: 1.5,
    borderRadius: 5,
    backgroundColor: 'white',
    marginTop: 8,
  },
  data: {
    color: 'black',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 10,
  },
});

export default Filter;
