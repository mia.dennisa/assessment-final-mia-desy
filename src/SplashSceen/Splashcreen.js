import {Image, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import Logo from '../Image/Bankt.png';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('Login');
    }, 1000);
  });
  return (
    <View
      style={{
        backgroundColor: '#FFFFFF',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Image
        source={Logo}
        style={{height: 160, width: 200, resizeMode: 'center'}}
        title={'Bankkok'}
      />
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontSize: 30,
            color: '#00b8e6',
            fontWeight: 'bold',
          }}>
          Bankkok
        </Text>
      </View>
    </View>
  );
};

export default SplashScreen;
