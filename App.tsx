import {View, Text} from 'react-native';
import React from 'react';
import {Provider} from 'react-redux';
import Router from './src/Router/Router';
import storeRedux from './src/Redux/Store';

const App = () => {
  return (
    <Provider store={storeRedux}>
      <Router />
    </Provider>
  );
};

export default App;
